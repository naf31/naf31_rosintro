# AUTHOR: Noah Falbaum
# DESCRIPTION: This code commands the UR5e robot to draw the initials NAF on a whiteboard
#
# CONTRIBUTORS: This program is adapted from code found in the MoveIt tutorials
#               https://ros-planning.github.io/moveit_tutorials/doc/move_group_python_interface/move_group_python_interface_tutorial.html#

## SETUP
# Imports and ROS
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# Initialize the ROS C++ components and pass command line arguments to ROS
moveit_commander.roscpp_initialize(sys.argv)

# Initialize a new ROS node with the name Move_group_python_interface_tutorial
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

# Create RobotCommander object to interface with robot
robot = moveit_commander.RobotCommander()

# Create a PlanningSceneInterface object to interface with the planning scene
scene = moveit_commander.PlanningSceneInterface()

# Define the name of the joint group that we want to control
group_name = "manipulator"

# Create a MoveGroupCommander object for the specified group
move_group = moveit_commander.MoveGroupCommander(group_name)

# Create a publisher to display planned trajectories for visualization
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# Pring the name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
print("============ Planning frame: %s" % planning_frame)

# Print the name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
print("============ End effector link: %s" % eef_link)

# Print list of all the groups in the robot:
group_names = robot.get_group_names()
print("============ Available Planning Groups:", robot.get_group_names())

# Sometimes for debugging it is useful to print the entire state of the
# robot:
print("============ Printing robot state")
print(robot.get_current_state())
print("")

# ______________________________________

## MANIPULATION
# We get the joint values from the group and change some of the values:
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = tau / 4
joint_goal[1] = -tau / 4
joint_goal[2] = tau / 2.5
joint_goal[3] = tau / 2
joint_goal[4] = 0
joint_goal[5] = 0

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()

# CARTESIAN GOAL CODE

waypoints = []
scale = 1
wpose = move_group.get_current_pose().pose


def print_joint_values():
    # Get the current joint values
    current_joint_values = move_group.get_current_joint_values()

    # Print the joint angle values
    print("Joint Angle Values:")
    for i, joint_value in enumerate(current_joint_values):
        print(f"Joint {i+1}: {joint_value:.4f} rads")


# KEY POSE REACHED -- PRINT JOINT VALUES
print_joint_values()


# Sequence of movements to draw the N
def draw_N():
    # Move up by 0.3
    wpose.position.z += scale * 0.3  # First move up (z)
    waypoints.append(copy.deepcopy(wpose))

    # Move right by 0.2 and down by 0.3 (diagonal)
    wpose.position.y += scale * 0.2  #  move forward in (y)
    wpose.position.z -= scale * 0.3  #  move down in (z)
    waypoints.append(copy.deepcopy(wpose))

    # Move up by 0.3
    wpose.position.z += scale * 0.3  #  move up (z)
    waypoints.append(copy.deepcopy(wpose))

    # Return Home
    wpose.position.y -= scale * 0.2
    wpose.position.z -= scale * 0.3
    waypoints.append(copy.deepcopy(wpose))


# Sequence of movements to draw the A
def draw_A():
    # Move right by 0.1 and up by 0.3 (diagonal)
    wpose.position.y += scale * 0.1
    wpose.position.z += scale * 0.3
    waypoints.append(copy.deepcopy(wpose))

    # Move right by 0.1 and down by 0.3 (diagonal)
    wpose.position.y += scale * 0.1
    wpose.position.z -= scale * 0.3
    waypoints.append(copy.deepcopy(wpose))

    # Move left by 0.05 and up by 0.15 (diagonal)
    wpose.position.y -= scale * 0.05
    wpose.position.z += scale * 0.15
    waypoints.append(copy.deepcopy(wpose))

    # Move left by 0.1
    wpose.position.y -= scale * 0.1
    waypoints.append(copy.deepcopy(wpose))

    # Return Home
    wpose.position.y -= scale * 0.05
    wpose.position.z -= scale * 0.15
    waypoints.append(copy.deepcopy(wpose))


# Sequence of Movements to draw the F
def draw_F():
    # Move up by 0.15
    wpose.position.z += scale * 0.15
    waypoints.append(copy.deepcopy(wpose))

    # Move right by 0.1
    wpose.position.y += scale * 0.1
    waypoints.append(copy.deepcopy(wpose))

    # Move left by 0.1
    wpose.position.y -= scale * 0.1
    waypoints.append(copy.deepcopy(wpose))

    # Move up by 0.15
    wpose.position.z += scale * 0.15
    waypoints.append(copy.deepcopy(wpose))

    # Move right by 0.2
    wpose.position.y += scale * 0.2
    waypoints.append(copy.deepcopy(wpose))

    # Return Home
    wpose.position.y -= scale * 0.2
    wpose.position.z -= scale * 0.3
    waypoints.append(copy.deepcopy(wpose))


#
def draw_letters():
    draw_N()
    draw_A()
    draw_F()

    # Interpolate cartesian path at resolution of 1 cm
    (plan, fraction) = move_group.compute_cartesian_path(
        waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
    )  # jump_threshold

    move_group.execute(plan, wait=True)


draw_letters()

# print_joint_values()
