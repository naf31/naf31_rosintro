rosservice call /turtle1/set_pen 0 0 0 5 1
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[-4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /turtle1/set_pen 255 0 0 5 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.0, -4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /turtle1/set_pen 0 0 0 5 1
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.0, -4.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /turtle1/set_pen 0 255 0 5 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[6.0, 0.0, 0.0]' '[0.0, 0.0, 6.28]'

rosservice call /turtle1/set_pen 0 0 0 5 1
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
